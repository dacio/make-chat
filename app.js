const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');

const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

const chatListener = require('./sockets/chat');

io.on('connection', (socket) => {
  console.log('🔌 New user connected! 🔌');
  chatListener(io, socket);
});

app.engine('hbs', exphbs({
  extname: '.hbs',
  layoutsDir: path.join(__dirname, 'views/layouts/'),
  partialsDir: path.join(__dirname, 'views/partials'),
  // defaultLayout: 'main',
}));
app.set('view engine', 'hbs');

app.use('/public', express.static('public'));

app.get('/', (req, res) => {
  res.render('index');
});

server.listen(process.env.PORT || 3000, () => {
  console.log('Server listening on Port 3000');
});

module.exports = server;
