$(document).ready(function ready() {
  var socket = io.connect();
  var currentUser = '';

  socket.emit('get online users');
  socket.emit('user changed channel', 'General');

  function showUsers(onlineUsers) {
    $('.usersOnline').empty();

    Object.keys(onlineUsers).forEach(function eachUser(username) {
      $('.usersOnline').append('<p class="userOnline">' + username + '</p>');
    });
  }

  socket.on('get online users', showUsers);

  socket.on('new user', function newUser(username) {
    console.log('✋ ' + username + ' has joined the chat! ✋');
    $('.usersOnline').append('<div class="userOnline">' + username + '</div>');
  });

  socket.on('new message', function recieveMsg(data) {
    var currentChannel = $('.channel-current').text();

    if (currentChannel === data.channel) {
      $('.messageContainer').append(
        '<div class="message">'
          + '<p class="messageUser">' + data.sender + ': </p>'
          + '<p class="messageText">' + data.message + '</p>'
        + '</div>'
      );
    }
  });

  socket.on('user has left', showUsers);

  socket.on('new channel', function addChannel(newChannel) {
    $('.channels').append('<div class="channel">' + newChannel + '</div>');
  });

  socket.on('user changed channel', function changeChannel(data) {
    $('.channel-current').addClass('channel');
    $('.channel-current').removeClass('channel-current');
    $('.channel:contains(\'' + data.channel + '\')').addClass('channel-current');
    $('.channel-current').removeClass('channel');
    $('.message').remove();
    data.messages.forEach(function addMessage(message) {
      $('.messageContainer').append(
        '<div class="message">'
          + '<p class="messageUser">' + message.sender + ': </p>'
          + '<p class="messageText">' + message.message + '</p>'
        + '</div>'
      );
    });
  });

  $('#createUserBtn').click(function newUser(e) {
    var username = $('#usernameInput').val();
    e.preventDefault();

    if (username.length > 0) {
      socket.emit('new user', username);
      currentUser = username;
      $('.usernameForm').remove();
      $('.mainContainer').css('display', 'flex');
    }
  });

  $('#sendChatBtn').click(function sendMsg(e) {
    var channel = $('.channel-current').text();
    var message = $('#chatInput').val();
    e.preventDefault();

    if (message.length > 0) {
      socket.emit('new message', {
        sender: currentUser,
        message: message,
        channel: channel
      });
      $('#chatInput').val('');
    }
  });

  $('#newChannelBtn').click(function createChannel() {
    var newChannel = $('#newChannelInput').val();

    if (newChannel.length > 0) {
      socket.emit('new channel', newChannel);
      $('#newChannelInput').val('');
    }
  });

  $(document).on('click', '.channel', function joinChannel(e) {
    var newChannel = e.target.textContent;
    socket.emit('user changed channel', newChannel);
  });
});
