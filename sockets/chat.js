const onlineUsers = {};
const channels = { General: [] };

module.exports = (io, socket) => {
  let socketUsername;

  socket.on('new user', (username) => {
    onlineUsers[username] = socket.id;
    // console.dir(onlineUsers);
    // socket['username'] = username;
    socketUsername = username;
    console.log(`✋ ${username} has joined that chat! ✋`);

    io.emit('new user', username);
  });

  socket.on('new message', (data) => {
    channels[data.channel].push({ sender: data.sender, message: data.message });
    io.to(data.channel).emit('new message', data);
  });

  socket.on('get online users', () => {
    console.log(onlineUsers);
    socket.emit('get online users', onlineUsers);
  });

  socket.on('disconnect', () => {
    delete onlineUsers[socketUsername];
    io.emit('user has left', onlineUsers);
  });

  socket.on('new channel', (newChannel) => {
    channels[newChannel] = [];
    socket.join(newChannel);
    io.emit('new channel', newChannel);
    socket.emit('user changed channel', {
      channel: newChannel,
      messages: channels[newChannel],
    });
  });

  socket.on('user changed channel', (newChannel) => {
    socket.join(newChannel);
    socket.emit('user changed channel', {
      channel: newChannel,
      messages: channels[newChannel],
    });
  });
};
